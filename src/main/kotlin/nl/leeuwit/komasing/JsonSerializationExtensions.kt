package nl.leeuwit.komasing

import java.io.ByteArrayOutputStream
import java.io.StringWriter
import java.util.*
import javax.json.Json
import javax.json.JsonObject

fun MapSerializable.toJsonObject(): JsonObject {
    val map = if (this is Polymorphic) mapOf("@class" to javaClass.name) + toMap() else toMap()
    validate(map)
    return Json.createObjectBuilder(map).build()
}

fun MapSerializable.toJsonString(): String = StringWriter().use {
    Json.createWriter(it).writeObject(toJsonObject())
    it.toString()
}

fun MapSerializable.toJsonBytes(): ByteArray = ByteArrayOutputStream().use {
    Json.createWriter(it).writeObject(toJsonObject())
    it.toByteArray()
}

private fun validate(obj: Any?) {
    val stack = Stack<Any?>()
    stack.push(obj)
    while (!stack.empty()) {
        val value = stack.pop()
        if (value == null) {
            // do nothing
        } else if (value is Collection<*>) {
            value.forEach { stack.push(it) }
        } else if (value is Map<*, *>) {
            value.keys.forEach { if (it !is String) throw IllegalArgumentException("Map should only contain String keys.") }
            value.values.forEach { stack.push(it) }
        } else if (!(value is Number || value is Boolean || value is String)) {
            throw IllegalArgumentException("Not a Number, Boolean or String: ${value.javaClass}")
        }
    }
}