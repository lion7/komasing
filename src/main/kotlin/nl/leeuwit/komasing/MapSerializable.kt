package nl.leeuwit.komasing

interface MapSerializable {

    fun toMap(): Map<String, *>

}