package nl.leeuwit.komasing

import java.io.ByteArrayInputStream
import java.io.StringReader
import java.math.BigDecimal
import javax.json.Json
import javax.json.JsonArray
import javax.json.JsonNumber
import javax.json.JsonObject
import javax.json.JsonString
import javax.json.JsonValue

fun <T> MapDeserializable<T>.fromJsonObject(jsonObject: JsonObject): T = fromMap(handle(jsonObject))

fun <T> MapDeserializable<T>.fromJsonString(jsonString: String): T = StringReader(jsonString).use {
    Json.createReader(it).readObject()
}.let { fromJsonObject(it) }

fun <T> MapDeserializable<T>.fromJsonBytes(jsonBytes: ByteArray): T = ByteArrayInputStream(jsonBytes).use {
    Json.createReader(it).readObject()
}.let { fromJsonObject(it) }

private fun handle(value: JsonValue): Any? = when (value.valueType) {
    JsonValue.ValueType.NULL -> null
    JsonValue.ValueType.TRUE -> true
    JsonValue.ValueType.FALSE -> false
    JsonValue.ValueType.NUMBER -> (value as JsonNumber).numberValue().let { if (it is BigDecimal) it.toDouble() else it }
    JsonValue.ValueType.STRING -> (value as JsonString).string
    JsonValue.ValueType.ARRAY -> handle(value.asJsonArray())
    JsonValue.ValueType.OBJECT -> handle(value.asJsonObject())
    else -> throw IllegalArgumentException("JsonValue.valueType cannot be null")
}

private fun handle(value: JsonArray): List<Any?> = value.asJsonArray().map { handle(it) }

private fun handle(value: JsonObject): Map<String, Any?> = value.asJsonObject().mapValues { handle(it.value) }