package nl.leeuwit.komasing

interface MapDeserializable<T> {

    companion object : MapDeserializable<Any?> {
        override fun fromMap(map: Map<*, *>): Any? {
            return fromMap(map, Thread.currentThread().contextClassLoader)
        }

        fun fromMap(map: Map<*, *>, classLoader: ClassLoader): Any? {
            val className = map["@class"] as? String ?: throw IllegalArgumentException("Class name not available")
            val javaClass = classLoader.loadClass(className)
            return fromMap(map, javaClass)
        }

        fun fromMap(map: Map<*, *>, javaClass: Class<*>): Any? {
            val companion = javaClass.getField("Companion").get(null) as? MapDeserializable<*>
            if (companion?.javaClass?.name != "${javaClass.name}\$Companion") {
                throw IllegalArgumentException("$javaClass should have a companion object that implements MapDeserializable<${javaClass.simpleName}>")
            }
            return companion.fromMap(map)
        }
    }

    fun fromMap(map: Map<*, *>): T

}