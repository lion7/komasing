import org.gradle.kotlin.dsl.extra
import org.gradle.kotlin.dsl.repositories
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.3.10"
    id("com.zoltu.git-versioning") version "3.0.3"
    id("org.javamodularity.moduleplugin") version "1.2.1"
}

group = "nl.leeuwit"

repositories {
    jcenter()
    maven("https://dl.bintray.com/kotlin/kotlin-eap")
}

dependencies {
    api("org.jetbrains.kotlin:kotlin-stdlib-jdk8:1.3.20-eap-52:modular") {
        isTransitive = false
    }
    api("org.jetbrains.kotlin:kotlin-stdlib-jdk7:1.3.20-eap-52:modular") {
        isTransitive = false
    }
    api("org.jetbrains.kotlin:kotlin-stdlib:1.3.20-eap-52:modular") {
        isTransitive = false
    }
    compileOnly("org.glassfish:javax.json:1.1.4")

    testImplementation("org.junit.jupiter:junit-jupiter-api:5.3.2")
    testImplementation("org.junit.jupiter:junit-jupiter-engine:5.3.2")
}

tasks {
    withType<Wrapper> {
        distributionType = Wrapper.DistributionType.ALL
        gradleVersion = "5.0"
    }

    compileJava {
        destinationDir = compileKotlin.get().destinationDir // needed for jigsaw support
    }

    compileKotlin {
        kotlinOptions {
            jvmTarget = "1.8"
        }
    }

    compileTestJava {
        destinationDir = compileTestKotlin.get().destinationDir // needed for jigsaw support
    }

    compileTestKotlin {
        kotlinOptions {
            jvmTarget = "1.8"
        }
    }

    test {
        useJUnitPlatform()
    }
}